import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [

  {path:'',            component:HeaderComponent},
  {path:'home',            component:HeaderComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},
];

@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
