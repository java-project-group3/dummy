import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  protected aFormGroup: FormGroup;
  loginForm: FormGroup;
user : any;
isCaptchaValid: boolean = false;
captchaResolved: boolean = false;
siteKey: string = "6Lev5GcpAAAAAF-jW9OzsG-Im3j0aQE1nBxAVyhf";
  constructor(private formBuilder: FormBuilder,private service :UserService,private router :Router,private toastr:ToastrService) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
  }

   async loginSubmit() {
    const email = this.loginForm.get('email')?.value;
    const password = this.loginForm.get('password')?.value;
  
    if (email && password) {
      if (email.errors) {
        if (email.hasError('required')) {
          // Handle required error for email
        } else if (email.hasError('email')) {
          // Handle email format error
        }
      }
  
      if (password.errors) {
        if (password.hasError('required')) {
          // Handle required error for password
        } else if (password.hasError('minlength')) {
          // Handle minimum length error for password
        }
      }
    }
    await this.service.userLogin(email, password).then((data: any) => {
      console.log(data);
      this.user = data;
    });
    if (this.user!= null) {
      //this.service.setIsUserLoggedIn();        
      //localStorage.setItem("emailId", loginForm.emailId);
      this.toastr.success("Login Success");
      this.router.navigate(['home']);
    } else {

      this.toastr.error("Invalid Credentials");
    }
  }
  onCaptchaResolved(event: any) {
    // Handle captcha resolved event
    this.isCaptchaValid = true;
    this.captchaResolved = true;
  }
}  