import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  
  registerUser(user: any): any {
    return this.http.post('http://localhost:8085/addUser', user);
  }
  userLogin(emailId: string, password: string): any {
    return this.http.get(`http://localhost:8085/userLogin/${emailId}/${password}`).toPromise();
  }
  
  
}
