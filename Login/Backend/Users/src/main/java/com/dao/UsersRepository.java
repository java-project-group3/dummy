package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {

    @Query("from Users where concat(firstName, ' ', lastName) = :userName")
    Users findByName(@Param("userName") String userName);
    
    @Query("from Users where emailId=:emailId")
	Users findByEmailId(@Param("emailId") String emailId);
    
	@Query("from Users where emailId = :emailId and password = :password")
	Users userLogin(@Param("emailId") String emailId, @Param("password") String password);

}
