package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Users;






@Service
public class UsersDao {

	@Autowired
	UsersRepository usersRepository;
	@Autowired
	private JavaMailSender mailSender;

	public List<Users> getUsers() {
		return usersRepository.findAll();
	}

	public Users getUserById(int userId) {
		return usersRepository.findById(userId).orElse(null);
	}

	public Users getUserByName(String userName) {
		return usersRepository.findByName(userName);
	}
      private void sendWelcomeEmail(Users user) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(user.getEmailId());
		message.setSubject("Welcome to our website");
		message.setText("Dear " + user.getFirstName() + ",\n\n"
				+ "Thank you for registering Welcome to Urban Nest, your gateway to comfortable and hassle-free living in urban areas!"+"\n"+

                  "We are thrilled to have you join our community. As a member of Urban Nest, you'll enjoy exclusive access to a wide range of PG accommodations, tailored to suit your needs. ");

		mailSender.send(message);
	}
    //adding user By encrypting password
	public Users addUsers(Users users) {
		  BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
	        String encryptedPwd = bcrypt.encode(users.getPassword());
	        users.setPassword(encryptedPwd);
	        Users savedUser =usersRepository.save(users);
	        //Send a welcome email
			sendWelcomeEmail(savedUser);
		return savedUser;
	}
   //UserLogin
	public Users userLogin(String emailId, String password) {
	    // Find the employee by emailId
	    Users user = usersRepository.findByEmailId(emailId);

	    // Check if employee exists and the provided password matches the stored password
	    if (user != null) {
	        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	        if (passwordEncoder.matches(password, user.getPassword())) {
	            // Passwords match, return the employee
	            return user;
	        }
	    }

	    // Return null if login fails
	    return null;
	}
	
	public Users updateUser(Users users) {
		return usersRepository.save(users);
	}

	public void deleteUserById(int userId) {
		usersRepository.deleteById(userId);
	}
	
	
}
